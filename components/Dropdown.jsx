import React from "react";
import { Divider, Text } from "react-native-paper";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements";
import { useState } from "react";

function Dropdown(props) {
    const [visible, setVisible] = useState(true);

    const toggleDropdown = () => {
        setVisible(!visible);
    };

    const renderDropdown = () => {
        if (visible) {
            return (
                props.piercings.map((piercing) => (<Text style={styles.piercings} key={piercing.key} variant="displaySmall" onPress={() => props.navigation.navigate("Piercing", { name: piercing.name })}>{piercing.name}</Text>))
            );
        }
    };

    if(visible) {
        return(
            <TouchableOpacity
                style={styles.dropped}
                onPress={toggleDropdown}
            >
                {renderDropdown()}
                <Text style={styles.buttonText}>{props.label}</Text>
                <Icon type='font-awesome' name='chevron-down'/>
            </TouchableOpacity>
        )
    }
    else {
        return(
            <TouchableOpacity
                style={styles.button}
                onPress={toggleDropdown}
            >
                <Text style={styles.buttonText}>{props.label}</Text>
                <Icon type='font-awesome' name='chevron-up'/>
            </TouchableOpacity>
        )
    }
    
}

const styles = StyleSheet.create({
    centered: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    button: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: '90%',
        paddingHorizontal: 10,
        zIndex: 1
    },
    dropped: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 90,
        width: '90%',
        paddingHorizontal: 10,
        zIndex: 1
    },
    buttonText: {
        flex: 1,
        textAlign: 'center',
        fontSize: 30
    },
    dropdown: {
        position: 'absolute',
        top: 50,
        marginTop: 10,
        fontSize: 20
    },
    piercings: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
        lineHeight: 50,
        position: 'absolute',
        top: 40,
        marginTop: 20
    },
})

export default Dropdown;