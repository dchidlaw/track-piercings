import React from "react";
import { Text, Avatar } from "react-native-paper";
import { View, StyleSheet } from "react-native";

function Battery() {
    return(
        <View style={[styles.container, styles.topView]}>
            <Avatar.Icon size={60} icon={"battery"} />
            <Text style={styles.batteryPercent}> 70%</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    topView: {
        justifyContent: "center",
        alignItems: "center"
    },
    container: {
        flexDirection:'row',
        flexWrap: 'wrap',
        marginLeft: 40
    },
    batteryPercent: {
       fontSize: 20 
    }
});
export default Battery;

