import React from "react";
import { View, StyleSheet, ImageBackground } from "react-native";
import { BackgroundImage } from "react-native-elements/dist/config";
import { Divider, Text } from "react-native-paper"
import Dropdown from "./Dropdown";


function MyPiercings({navigation}) {
    

    return(
        <View style={styles.centered}>
            <ImageBackground source={require("../assets/background.jpg")} resizeMode="cover" style={styles.image}>
                <Text style={styles.welcomeMessage} variant="displayLarge">My Piercings</Text>
                {/* <Text style={styles.subheader} variant="displayMedium">Healing:</Text>
                <Text style={styles.piercings} variant="displaySmall" onPress={() => navigation.navigate("Piercing", { name: 'Septum' })}>Septum</Text> */}
                <Dropdown label="Healing" piercings={[{key: 1, name: "Septum"}]} navigation={navigation}/>
                <Divider style={styles.divide} bold="true"/>
                <Dropdown label="Healed" piercings={[{key: 1, name: "Nose Ring"}]} navigation={navigation}/>
                {/* <Text style={styles.subheader} variant="displayMedium">Healed:</Text>
                <Text style={styles.piercings} variant="displaySmall">Nose Ring</Text>
                <Text style={styles.piercings} variant="displaySmall">Monroe</Text> */}
                <Divider style={styles.divide} bold="true"/>
                {/* <Text style={styles.subheader} variant="displayMedium">Desired Piercings:</Text>
                <Text style={styles.piercings} variant="displaySmall">Guages</Text> */}
                <Dropdown style={styles.navTitle} label="Desired Piercings" piercings={[{key:1, name: "Guages"}]} navigation={navigation}/>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    centered: {
      flex: 1
    },
    navTitle: {
      textAlign: 'center',
      justifyContent: 'center',
      marginTop: 40,
      marginBottom: 40
    },
    welcomeMessage: {
      fontFamily: "Helvetica-Light",
      color: "blue",
      textAlign: 'center',
      justifyContent: 'top',
      marginBottom: 50,
      verticalalign: "text-top"
    },
    subheader: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
    },
    piercings: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
        lineHeight: 40

    },
    divide: {
        marginTop: 30,
        marginBottom: 30,
        backgroundColor: "blue"
    },
    image: {
        flex: 1,
        justifyContent: "center"
    }

});

export default MyPiercings;