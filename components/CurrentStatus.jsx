import React from "react";
import { View, StyleSheet, Pressable, ImageBackground } from "react-native";
import { Text, SegmentedButtons } from "react-native-paper";

function CurrentStatus({navigation}) {
    return(
            <View style={styles.topView}>
                <Text variant="headlineLarge">Current Status</Text>
                <View style={styles.container}>
                    <View style={[styles.circle, {backgroundColor: 'red'}]} />
                    <View style={[styles.circle]} />
                    <View style={[styles.circle]} />
                </View>
                <Text variant="headlineSmall">Problems:</Text>
                <Text variant="headlineSmall">Infection Detected!</Text>
            </View>
    );
}

const styles = StyleSheet.create({
    topView: {
        justifyContent: "center",
        alignItems: "center"
    },
    container: {
        flexDirection:'row',
        flexWrap: 'wrap'
    },
    circle: {
        width: 44,
        height: 44,
        borderRadius: 44 / 2,
        borderColor: 'black',
        borderWidth: 3,
        marginHorizontal: 5,
        justifyContent:'space-evenly',
      }
})
export default CurrentStatus;