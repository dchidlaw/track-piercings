import React from "react";
import { Text, Avatar } from 'react-native-paper';
import { StyleSheet, View, TouchableOpacity, ImageBackground } from 'react-native';

function HomeScreen({ navigation }) {

    return( 
      <View style={styles.centered}>
        <ImageBackground source={require("../assets/background.jpg")} resizeMode="cover" style={styles.image}>
          <View style={styles.container}>
            <Avatar.Icon size={60} icon="human-greeting"></Avatar.Icon>
            <Text style={styles.welcomeMessage} variant="displayLarge">Welcome!</Text>
          </View> 
          <TouchableOpacity style={styles.container} onPress={() => navigation.navigate('What\'s New')}>
            <Avatar.Icon size={60} icon="newspaper"></Avatar.Icon>
            <Text style={styles.navTitle} variant="displayLarge">What's New</Text>
          </TouchableOpacity>  
          <TouchableOpacity style={styles.container} onPress={() => navigation.navigate('My Piercings')} >
            <Avatar.Icon size={60} icon="ring"></Avatar.Icon>
            <Text style={styles.navTitle} variant="displayLarge" >My Piercings</Text>
          </TouchableOpacity>
        </ImageBackground>
      </View>

    );

}

const styles = StyleSheet.create({
    centered: {
      flex: 1,
    },
    container: {
      flexDirection:'row',
      flexWrap: 'wrap',
      marginLeft: 0,
      marginTop: 50,
      justifyContent: "center"
    },
    navTitle: {
      fontFamily: "Helvetica-Light",
      textAlign: 'center',
      justifyContent: 'center',
      marginBottom: 50
    },
    welcomeMessage: {
      fontFamily: "Helvetica-Bold",
      color: "blue",
      textAlign: 'center',
      justifyContent: 'top',
      marginBottom: 50
    },
    image: {
      flex: 1,
      justifyContent: "center"
    },

  });
export default HomeScreen;