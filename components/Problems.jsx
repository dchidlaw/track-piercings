import React from "react";
import { DataTable, Text } from "react-native-paper";
import { View, StyleSheet, ScrollView, ImageBackground } from "react-native";
import TableRow from "./TableRow";
import dates from "../IssueArray";

function Problems() {

    return(
        <View style={styles.centered}>
            <ImageBackground source={require("../assets/background.jpg")} resizeMode="cover" style={styles.image}>
            <ScrollView>
                <Text style={styles.navTitle} variant="displayLarge">Problems</Text>
                <DataTable>
                    <DataTable.Header>
                        <DataTable.Title
                            sortDirection='descending'
                        >
                            Date
                        </DataTable.Title>
                        <DataTable.Title>Issues</DataTable.Title>
                        <DataTable.Title>Notes</DataTable.Title>
                    </DataTable.Header>
                    {dates.map((date) => (<TableRow key={date.id} color={date.issue} date={date.date} notes={date.notes}/>))}
                </DataTable>
            </ScrollView>
            </ImageBackground>
        </View>
    );
}


const styles = StyleSheet.create({
    centered: {
      flex: 1,
      backgroundColor: 'orchid',
      justifyContent: "center"
    },
    circle: {
        width: 35,
        height: 35,
        borderRadius: 35 / 2,
        borderColor: 'black',
        borderWidth: 3,
        marginHorizontal: 0,
        backgroundColor: "red",
    },
    navTitle: {
      textAlign: 'center',
      justifyContent: 'center',
      marginTop: 40,
      marginBottom: 40,
      color: "blue"
    },
    welcomeMessage: {
      fontFamily: "Helvetica-Light",
      backgroundColor: 'orchid',
      color: "blue",
      textAlign: 'center',
      justifyContent: 'top',
      marginBottom: 50,
      verticalalign: "text-top"
    },
    subheader: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
    },
    piercings: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
        lineHeight: 40

    },
    accordion: {
        textAlign: 'center',
        backgroundColor: 'orchid'
    },
    divide: {
        marginTop: 30,
        marginBottom: 30,
        backgroundColor: "blue"
    }

});

export default Problems;

