import React from "react";
import { Divider, Text} from "react-native-paper";
import { View, StyleSheet, TouchableOpacity, ImageBackground, ScrollView } from "react-native";
import CurrentStatus from "./CurrentStatus";
import SnoozeButton from "./SnoozeButton";
import Battery from "./Battery";

function Piercing({route, navigation}) {
    const name = route.params["name"];
    return(
        <View style={styles.centered}>
            <ImageBackground source={require("../assets/background.jpg")} resizeMode="cover" style={styles.image}>
            <Text variant="displayLarge" style={styles.welcomeMessage}>
                {name}
            </Text>
            <View style={[styles.topButtons]}>
                <SnoozeButton></SnoozeButton>
                <Battery></Battery>
            </View>
            <Divider style={styles.divide}/>
            <Text variant="displayMedium" style={styles.subheader}>Status: Healing</Text>
            <Text variant="displayMedium" style={styles.subheader}>Pierce Date:</Text>
            <Text variant="displayMedium" style={styles.piercings}>09/13/2022</Text>
            <Text variant="displayMedium" style={styles.subheader}>Avg. Healing Range:</Text>
            <Text variant="displayMedium" style={styles.piercings}>04/13/2023-06/13/2023</Text>
            <TouchableOpacity onPress={() => navigation.navigate("Problems")}>
                <CurrentStatus style={styles.statusCircles}></CurrentStatus>
            </TouchableOpacity>
            </ImageBackground>
        </View>
    );
    

}

const styles = StyleSheet.create({
    alignButton: {
        alignItems: "center",
        marginBottom: 20
    },
    topButtons: {
        flexDirection:'row',
        flexWrap: 'wrap',
        justifyContent: "center"
    },
    centered: {
      flex: 1, 
      backgroundColor: "orchid",
      justifyContent: "center"
    },
    welcomeMessage: {
      fontFamily: "Helvetica-Light",
      color: "blue",
      textAlign: 'center',
      justifyContent: 'top',
      marginBottom: 20,
      verticalalign: "text-top"
    },
    subheader: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
        fontSize:30,
        marginBottom: -8
    },
    piercings: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
        fontSize:25
    },
    accordion: {
        textAlign: 'center',
        backgroundColor: 'orchid'
    },
    divide: {
        marginTop: 0,
        marginBottom: 0,
        backgroundColor: "blue"
    },
    statusCircles: {
        justifyContent: 'center',
        textAlign: 'center',
        marginBottom: 20
    }

  });
export default Piercing;