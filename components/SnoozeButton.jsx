import React from "react";
import { Portal, Dialog, Paragraph, Button, IconButton } from "react-native-paper";
import { View, StyleSheet } from "react-native";
import { useState } from "react";

function SnoozeButton() {
    const [visible, setVisible] = useState(false);

    const [snoozed, setSnoozed] = useState(false);

    const showDialog = () => setVisible(true);
  
    const hideDialog = () => setVisible(false);

    if(snoozed) {
        return(
            <View style={styles.alignButton}>
                <IconButton size={40} icon={"sleep"} mode="contained" onPress={() => {
                    showDialog(); 
                    setSnoozed(!snoozed);
                }}></IconButton>
                <Portal>
                    <Dialog visible={visible && snoozed} onDismiss={hideDialog}>
                        <Dialog.Title>Alert</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph>Your Piercing has been snoozed</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                            <Button onPress={hideDialog}>Done</Button>
                        </Dialog.Actions>
                    </Dialog>
                    <Dialog visible={visible && !snoozed} onDismiss={hideDialog}>
                        <Dialog.Title>Alert</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph>Snooze has been disabled</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                            <Button onPress={hideDialog}>Done</Button>
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View >
        )
    }
    else {
        return(
            <View style={styles.alignButton}>
                <IconButton size={40} icon={"sleep"} mode="outlined" onPress={() => {
                    showDialog(); 
                    setSnoozed(!snoozed);
                }}></IconButton>
                <Portal>
                    <Dialog visible={visible && snoozed} onDismiss={hideDialog}>
                        <Dialog.Title>Alert</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph>Your Piercing has been snoozed</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                            <Button onPress={hideDialog}>Done</Button>
                        </Dialog.Actions>
                    </Dialog>
                    <Dialog visible={visible && !snoozed} onDismiss={hideDialog}>
                        <Dialog.Title>Alert</Dialog.Title>
                            <Dialog.Content>
                                <Paragraph>Snooze has been disabled</Paragraph>
                            </Dialog.Content>
                            <Dialog.Actions>
                            <Button onPress={hideDialog}>Done</Button>
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View >
        )
    }
    
}

const styles = StyleSheet.create({
    alignButton: {
        alignItems: "center",
        marginBottom: 20
    },
    centered: {
      flex: 1,
      backgroundColor: 'orchid',
      justifyContent: "center",
    },
    welcomeMessage: {
      fontFamily: "Helvetica-Light",
      backgroundColor: 'orchid',
      color: "blue",
      textAlign: 'center',
      justifyContent: 'top',
      marginBottom: 20,
      verticalalign: "text-top"
    },
    subheader: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
        fontSize:30,
        marginBottom: -8
    },
    piercings: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
        fontSize:25
    },
    accordion: {
        textAlign: 'center',
        backgroundColor: 'orchid'
    },
    divide: {
        marginTop: 0,
        marginBottom: 0,
        backgroundColor: "blue"
    },
    statusCircles: {
        justifyContent: 'center',
        textAlign: 'center',
        marginBottom: 20
    }

  });

export default SnoozeButton;