import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import WhatsNew from "./WhatsNew";
import MyPiercings from "./MyPiercings";
import HomeScreen from "./HomeScreen";
import Piercing from "./Piercing";
import Problems from "./Problems";

const Stack = createNativeStackNavigator();

function Home() {

    return(
        <Stack.Navigator initialRouteName="HomeScreen">
            <Stack.Screen name="HomeScreen" component={HomeScreen} />
            <Stack.Screen name="What's New" component={WhatsNew} />
            <Stack.Screen name="My Piercings" component={MyPiercings} />
            <Stack.Screen name="Piercing" component={Piercing} />
            <Stack.Screen name="Problems" component={Problems} />
        </Stack.Navigator>
    )
}

export default Home;