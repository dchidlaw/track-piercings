import React from "react";
import { DataTable, TextInput } from "react-native-paper";
import { StyleSheet, View } from "react-native";
import { useState } from "react";

function TableRow(props) {

    const [text, setText] = useState(props.notes);
    return(
        <DataTable.Row style={styles.row}>
            <DataTable.Cell>{props.date}</DataTable.Cell>
            <DataTable.Cell>
                <GetCircle color = {props.color}></GetCircle>
            </DataTable.Cell>
            <TextInput
                key="textinput1"
                placeholder="Notes"
                value={text}
                onChangeText={setText}
                style={styles.inputCell}/>
        </DataTable.Row>
    );
}

function GetCircle(props) {
    switch (props.color) {
        case "Red":
            return(
                <View style={[styles.circle, styles.red]} />
            );
        case "Yellow":
            return(
                <View style={[styles.circle, styles.yellow]} />
            );
        case "Green":
            return(
                <View style={[styles.circle, styles.green]} />
            );
        default:
            return(
                <View style={[styles.circle]} />
            );
    }
}

const styles = StyleSheet.create({
    inputCell: {
        width: 135,
        height: 60, 
        margin: 0
    },
    row: {
        height: 70
    },
    red: {
        backgroundColor: "red"
    },
    yellow: {
        backgroundColor: "yellow",
    },
    green: {
        backgroundColor: "green",
    },
    centered: {
      flex: 1,
      backgroundColor: 'orchid',
      justifyContent: "center",
    },
    circle: {
        width: 35,
        height: 35,
        borderRadius: 35 / 2,
        borderColor: 'black',
        borderWidth: 3,
        marginHorizontal: 0,
        
    },
    navTitle: {
      backgroundColor: 'orchid',
      textAlign: 'center',
      justifyContent: 'center',
      marginTop: 40,
      marginBottom: 40
    },
    welcomeMessage: {
      fontFamily: "Helvetica-Light",
      backgroundColor: 'orchid',
      color: "blue",
      textAlign: 'center',
      justifyContent: 'top',
      marginBottom: 50,
      verticalalign: "text-top"
    },
    subheader: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
    },
    piercings: {
        fontFamily: "Helvetica-Light",
        textAlign: 'center',
        lineHeight: 40

    },
    accordion: {
        textAlign: 'center',
        backgroundColor: 'orchid'
    },
    divide: {
        marginTop: 30,
        marginBottom: 30,
        backgroundColor: "blue"
    }

});
export default TableRow