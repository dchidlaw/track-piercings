const dates = [
    {
        id: 1,
        date: "10/31/2022",
        issue: "Red",
        notes: ""
    },
    {
        id: 2,
        date: "10/30/2022",
        issue: "Yellow",
        notes: "Needed to clean"
    },
    {
        id: 3,
        date: "10/29/2022",
        issue: "Yellow",
        notes: "Needed to clean"
    },
    {
        id: 4,
        date: "10/28/2022",
        issue: "Yellow",
        notes: ""
    },
    {
        id: 5,
        date: "10/27/2022",
        issue: "Yellow",
        notes: ""
    }
];

export default dates;