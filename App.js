import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './components/HomeScreen';
import { Provider as PaperProvider } from 'react-native-paper';
import WhatsNew from './components/WhatsNew';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MyPiercings from './components/MyPiercings';
import Home from './components/Home';

const Tab = createMaterialBottomTabNavigator();
const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <PaperProvider>
      <NavigationContainer>
        <Tab.Navigator initialRouteName="Home">
          <Tab.Screen name="Home" component={Home} />
          <Tab.Screen name="My Piercings" component={MyPiercings}/>    
        </Tab.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
}


